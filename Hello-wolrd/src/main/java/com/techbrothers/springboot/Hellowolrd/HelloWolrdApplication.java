package com.techbrothers.springboot.Hellowolrd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@SpringBootApplication
public class HelloWolrdApplication {
    
        @GetMapping("/hello-world")
        public String helloWorld(){
            return "Welcome to Spring boot";
        }

	public static void main(String[] args) {
		SpringApplication.run(HelloWolrdApplication.class, args);
	}

}

